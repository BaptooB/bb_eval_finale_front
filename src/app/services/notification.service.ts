import { Injectable } from "@angular/core";

export enum Pages  {
    login,
    register,
    home,
    createQuestion,
    question,
    userQuestion
};

@Injectable({
        providedIn: "root",
})

export class NotificationService{

    isLoggedIn: boolean = false;
    loginPageActive: boolean = false;
    registerPageActive:boolean = false;
    homePageActive:boolean = true
    createPageActive: boolean = false;
    questionPageActive: boolean = false;
    userPageActive: boolean = false;

    constructor(){
        
    }

    public login(){
        this.isLoggedIn = true;
    }

    public logout(){
        this.isLoggedIn = false;
    }

    public updateCurrentPage(page: Pages){
        
    }

    // TO DO : REFACTO
    public updatePage(page: Pages){
        switch(page){
            case Pages.home: this.homePageActive = true;
                            this.loginPageActive = false;
                            this.registerPageActive = false;
                            this.createPageActive = false;
                            this.questionPageActive = false;
                            this.userPageActive = false;
                            break;
            case Pages.register: this.homePageActive = false;
                            this.loginPageActive = false;
                            this.registerPageActive = true;
                            this.createPageActive = false;
                            this.questionPageActive = false;
                            this.userPageActive = false;
                            break;
            case Pages.login: this.homePageActive = false;
                            this.loginPageActive = true;
                            this.registerPageActive = false;
                            this.createPageActive = false;
                            this.questionPageActive = false;
                            this.userPageActive = false;
                            break;
            case Pages.createQuestion: this.homePageActive = false;
                            this.loginPageActive = false;
                            this.registerPageActive = false;
                            this.createPageActive = true;
                            this.questionPageActive = false;
                            this.userPageActive = false;
                            break;
            case Pages.question: this.homePageActive = false;
                            this.loginPageActive = false;
                            this.registerPageActive = false;
                            this.createPageActive = false;
                            this.questionPageActive = true;
                            this.userPageActive = false;
                            break;
            case Pages.userQuestion: this.homePageActive = false;
                                this.loginPageActive = false;
                                this.registerPageActive = false;
                                this.createPageActive = false;
                                this.questionPageActive = false;
                                this.userPageActive = true;
                                break;
        
        
        }
    }
}