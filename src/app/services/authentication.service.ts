import { HttpClient, HttpHeaders} from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { CreateUserDTO } from "../models/createUserDTO.model";
import { User } from "../models/user.model";

@Injectable({
        providedIn: "root",
})

export class AuthenticationService{

    URL_USERS: string = "http://localhost:8080/api/users/";
    URL_LOGIN: string = "http://localhost:8080/api/users/login";
    URL_REGISTER: string = "http://localhost:8080/api/users/register";

    user?: User;

    constructor(public http: HttpClient, private router: Router){
        
    }

    public register(user: CreateUserDTO){
        return this.http.post(this.URL_REGISTER, user);
    }
    public authentification(){
        let reqHeader = new HttpHeaders({ 'Content-Type': 'application/json','No-Auth':'True' });

        this.http.post(this.URL_LOGIN, null, { headers:reqHeader, responseType: 'text'}).subscribe(
            {
                next: (res) => { 
                    this.router.navigate([''])                     
                },
                error: (error) => {
                localStorage.removeItem('username')
                localStorage.removeItem('password')
                alert("error")
                }
              })
    }

    public checkStorageToken(){
        return (localStorage.getItem('username') != null && localStorage.getItem('password') != null);
    }

    public getAuthentificationToken() {
        const usr = localStorage.getItem('username');
        const pwd = localStorage.getItem('password');
        return btoa(usr+':'+pwd);
      }

    public getCurrentUserName(){
        return localStorage.getItem('username');
    }

    public getUserByName(username: string): Observable<User>{
        return this.http.get<User>(this.URL_USERS + username)
    }

    public isSignaler(username: string): boolean {
        return this.getCurrentUserName() === username;
    }

    public isAuthor(username: string): boolean{
        return this.getCurrentUserName() === username;
    }

    public logout(){
        this.user = undefined;
    }
    
}