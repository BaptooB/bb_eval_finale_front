import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Observable, Subject} from "rxjs";
import { Answer } from "../models/answer.model";
import { CreateAnswerDTO } from "../models/createanswerDTO.model";
import { CreateQuestionDTO } from "../models/createquestionDTO.model";

import { DetailedQuestion } from "../models/detailedQuestion.model";
import { Language } from "../models/language.model";

import { Question } from "../models/question.model";
import { User } from "../models/user.model";


@Injectable({
        providedIn: "root",
})

export class HttpService{

    URL_QUESTIONS: string = "http://localhost:8080/api/questions";
    URL_ANSWERS: string = "http://localhost:8080/api/answers";
    URL_QUESTIONS_LANGUAGE: string = "http://localhost:8080/api/questions/languages?language=";
    URL_USER_QUESTIONS: string = "http://localhost:8080/api/questions/users?username=";
    URL_USER_ANSWERS: string = "http://localhost:8080/api/answers/users?username=";

    URL_LANGUAGES: string = "http://localhost:8080/api/maintenance/languages";
    URL_SIGNALED_QUESTIONS: string = "http://localhost:8080/api/signaled/questions";
    URL_SIGNALED_ANSWERS: string = "http://localhost:8080/api/signaled/answers";

    searchTitle: Subject<string> = new Subject();

    constructor(public http: HttpClient, private router: Router){
        
    }

    public getQuestions(title: string | null): Observable<Question[]>{
        let url = this.URL_QUESTIONS;
        if(title != null){
            url += '?title=' + title;
        }
        return this.http.get<Question[]>(url);
    }

    public getQuestionById(tid: string): Observable<DetailedQuestion>{
        return this.http.get<DetailedQuestion>(this.URL_QUESTIONS + '/' + tid);
    }

    public createNewAnswer(answer: CreateAnswerDTO, questionTid: string): Observable<Answer>{
        return this.http.post<Answer>(this.URL_QUESTIONS + '/' + questionTid, answer);
    }

    public createNewQuestion(question: CreateQuestionDTO): Observable<Question>{
        return this.http.post<Question>(this.URL_QUESTIONS, question);
    }

    public getUserQuestions(username : string): Observable<Question[]>{
        return this.http.get<Question[]>(this.URL_USER_QUESTIONS + username);
    }

    public getUserAnswers(username : string): Observable<Answer[]>{
        return this.http.get<Answer[]>(this.URL_USER_ANSWERS + username);
    }

    public getUnsolvedQuestions(): Observable<Question[]>{
        return this.http.get<Question[]>(this.URL_QUESTIONS + '/unsolved');
    }

    public getQuestionsByLanguage(language: string): Observable<Question[]>{
        return this.http.get<Question[]>(this.URL_QUESTIONS_LANGUAGE + language);
    }

    public getLanguages(): Observable<Language[]>{
        return this.http.get<Language[]>(this.URL_LANGUAGES);
    }

    public createNewLanguage(language: Language): Observable<Language>{
        return this.http.post<Language>(this.URL_LANGUAGES, language);
    }

    public signalAnswer(aTid: string): Observable<Answer>{
        return this.http.patch<Answer>(this.URL_ANSWERS + '/' + aTid, null);
    }

    public signalQuestion(qTid: string): Observable<Question>{
        return this.http.patch<Question>(this.URL_QUESTIONS + '/' + qTid, null);
    }

    public getSignaledQuestions(): Observable<Question[]>{
        return this.http.get<Question[]>(this.URL_SIGNALED_QUESTIONS);
    }

    public getSignaledAnswers(): Observable<Answer[]>{
        return this.http.get<Answer[]>(this.URL_SIGNALED_ANSWERS);
    }

    public deleteQuestion(qTid: string){
        return this.http.delete(this.URL_QUESTIONS + '/' + qTid);
    }

    public deleteAnswer(aTid: string){
        return this.http.delete(this.URL_ANSWERS + '/' + aTid);
    }


}