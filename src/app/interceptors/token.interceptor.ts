import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthenticationService } from '../services/authentication.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  URL_SIGNALED_QUESTIONS: string = "http://localhost:8080/api/signaled/questions";
  URL_SIGNALED_ANSWERS: string = "http://localhost:8080/api/signaled/answers";
 
    constructor(private authService: AuthenticationService){}

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if(httpRequest.method != "GET" && this.authService.checkStorageToken() || this.isSpecialGetURL(httpRequest.url) ){
      httpRequest = httpRequest.clone({ setHeaders: { 'Authorization': 'Basic ' + this.authService.getAuthentificationToken() } });
    }
    return next.handle(httpRequest);
  }

  private isSpecialGetURL(url: string): boolean{
    return (url === this.URL_SIGNALED_ANSWERS || url === this.URL_SIGNALED_QUESTIONS);
  }

}