import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpEvent, HttpRequest, HttpHandler, HttpErrorResponse } from '@angular/common/http';
import { catchError, Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';

@Injectable()
export class ErrorInterceptor implements HttpInterceptor {

    constructor(private router: Router) {

    }

    intercept(httpRequest: HttpRequest<any>, next: HttpHandler):
        Observable<HttpEvent<any>> {
        return next.handle(httpRequest)
            .pipe(
                catchError(
                    (error: HttpErrorResponse) => {
                        if (error.status === 401) {
                            this.router.navigate(['login']);
                        }
                        return throwError(() => new Error(error.status.toString()));
                    }
                )
            )
    }

}