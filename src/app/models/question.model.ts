export interface Question{
    tid?: string;
    title: string;
    content: string;
    timestamp: number;
    language: string;
    signaled: boolean;
    signaledBy: string;
    username: string;
    email: string;

}