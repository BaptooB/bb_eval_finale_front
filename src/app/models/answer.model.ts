export interface Answer{
    tid?: string;
    questionTid: string;
    content: string;
    timestamp: number;
    signaled: boolean;
    signaledBy: string;
    username: string;
    email: string;
    rating?: string[];
}