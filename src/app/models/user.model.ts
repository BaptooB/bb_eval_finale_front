export interface User{
    tid: string;
    username: string;
    email: string;
    password: string;
    authorities: Authority[];
}

interface Authority{
    authority: String;
}