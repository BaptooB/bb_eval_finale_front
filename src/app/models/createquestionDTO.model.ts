export interface CreateQuestionDTO{
    title: string;
    content: string;
    language: string;
}