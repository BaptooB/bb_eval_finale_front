import { Answer } from "./answer.model";
import { Question } from "./question.model";

export interface DetailedQuestion{
    question: Question;
    answers: Answer[];
}
