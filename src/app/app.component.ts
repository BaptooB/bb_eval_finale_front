import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationService } from './services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Zenika FAQ';

  loginPageActive: boolean = true;
  loggedIn: boolean = false;

  constructor(private router: Router, private notificationService: NotificationService) { }

  ngOnInit(): void {
    // if(this.authService.checkStorageToken()){
    //   this.notificationService.login();
    // }
  }

  public onNavEvent(event: boolean){
    this.loginPageActive = event;
    this.router.navigate(['login']);
  }

  public onNavLogoutEvent(event: boolean){
    this.loggedIn = event;
    this.router.navigate(['']);
  }
}
