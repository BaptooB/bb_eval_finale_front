import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Language } from 'src/app/models/language.model';
import { Question } from 'src/app/models/question.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService, Pages } from 'src/app/services/notification.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

  languages?: Language[];
  questions: Question[] = new Array();
  username: string | null = null;
  filterForm: FormGroup;
  

  constructor(private notifService: NotificationService, private httpService: HttpService, public authService: AuthenticationService) { 
    this.username = this.authService.getCurrentUserName();
    this.filterForm = new FormGroup({
      langFilter: new FormControl(Validators.required)
    });
    this.filterForm.controls['langFilter'].setValue('', {onlyself: true})
  }

  ngOnInit(): void {
    this.notifService.updatePage(Pages.home);
    this.httpService.getQuestions(null).subscribe(
      list => this.questions = list
    );
    this.httpService.searchTitle.subscribe(
      title => {
      this.httpService.getQuestions(title).subscribe(
        list => this.questions = list
      )}
    );
    this.httpService.getLanguages().subscribe(
      res => this.languages = res
    );

    this.filterForm.valueChanges.subscribe(
      changes => {
        changes.langFilter == '' ? this.onSignalEvent() : this.changeLangFilter(changes.langFilter);        
      });

  }

  public onSignalEvent(){
    this.httpService.getQuestions(null).subscribe(
      list => this.questions = list
    );
  }

  public changeLangFilter(language: string){
    this.httpService.getQuestionsByLanguage(language).subscribe(
      list => this.questions = list
    );
  }

}
