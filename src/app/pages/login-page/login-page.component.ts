import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService, Pages } from 'src/app/services/notification.service';

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.scss']
})
export class LoginPageComponent implements OnInit {

  constructor(private router: Router, private httpService: HttpService, private notifService: NotificationService, private authService : AuthenticationService) {

  }

  ngOnInit(): void {
    this.notifService.updatePage(Pages.login);
  }

  public submitLogin(f: NgForm) {
    localStorage.setItem('username', f.value.usr);
    localStorage.setItem('password', f.value.pwd);
    
    this.authService.authentification();
  }

}
