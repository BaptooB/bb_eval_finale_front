import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetailedQuestion } from 'src/app/models/detailedQuestion.model';
import { Question } from 'src/app/models/question.model';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService, Pages } from 'src/app/services/notification.service';

@Component({
  selector: 'app-question-page',
  templateUrl: './question-page.component.html',
  styleUrls: ['./question-page.component.scss']
})
export class QuestionPageComponent implements OnInit {

  detailedQuestion?: DetailedQuestion;

  constructor(private notifService: NotificationService,
    private activatedRoute: ActivatedRoute, private httpService: HttpService) { 

      this.initQuestion();
    }

  ngOnInit(): void {
    this.notifService.updatePage(Pages.createQuestion);
  }

  public initQuestion(){
    const tid = this.activatedRoute.snapshot.params['id'];
      this.httpService.getQuestionById(tid).subscribe(
        q => {   
          if(q.question.tid == tid){
            this.detailedQuestion = q;            
          }
        });

        if(!this.detailedQuestion){
          return;
        }
  }

}
