import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Answer } from 'src/app/models/answer.model';
import { Question } from 'src/app/models/question.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService, Pages } from 'src/app/services/notification.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  questions?: Question[];
  answers?: Answer[];
  isQuestionView: boolean = true;
  username: string = '';
  isAdmin: boolean = false;
  isSignaledQuestionView: boolean = false;
  filterForm: FormGroup;

  constructor(private notifService: NotificationService,
    private activatedRoute: ActivatedRoute, private httpService: HttpService, private authService: AuthenticationService) {

    this.filterForm = new FormGroup({
      signalFilter: new FormControl(Validators.required)
    });

    this.filterForm.controls['signalFilter'].setValue('all', { onlyself: true });

  }

  ngOnInit(): void {
    this.username = this.activatedRoute.snapshot.params['username'];
    this.initData();
    if (this.username === this.authService.getCurrentUserName()) {
      this.notifService.updatePage(Pages.userQuestion);
    }

    const username = this.authService.getCurrentUserName();
    if (username) {
      this.authService.getUserByName(username).subscribe(
        u => {
          this.isAdmin = u.authorities[0].authority === 'ADMIN';
        }
      )
    };

    this.filterForm.valueChanges.subscribe(
      changes => { this.initData();});
  }

  public initData(){
    if(this.isQuestionView){
      this.filterForm.value.signalFilter == 'all' ? this.initQuestions() : this.initSignaledQuestions();
    } else {
      this.filterForm.value.signalFilter == 'all' ? this.initAnswers() : this.initSignaledAnswers();
    }
  }
  public initQuestions() {
    
    this.httpService.getUserQuestions(this.username).subscribe(
      qList => {
        this.questions = qList
      });

    if (!this.questions) {
      return;
    }
  }

  public initAnswers() {
    this.httpService.getUserAnswers(this.username).subscribe(
      aList => {
        this.answers = aList
      });

    if (!this.answers) {
      return;
    }
  }

  public changeDiplay(setIsQuestion: boolean) {
    this.isQuestionView = setIsQuestion;
    this.initData();
  }

  public initSignaledQuestions(){
    this.httpService.getSignaledQuestions().subscribe(
      list => this.questions = list
    );
  }

  public initSignaledAnswers(){
    this.httpService.getSignaledAnswers().subscribe(
      list => this.answers = list
    );
  }
}
