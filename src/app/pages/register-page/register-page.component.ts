import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateUserDTO } from 'src/app/models/createUserDTO.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { NotificationService, Pages } from 'src/app/services/notification.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.scss']
})
export class RegisterPageComponent implements OnInit {

  constructor(private notifService: NotificationService, private authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    this.notifService.updatePage(Pages.register);
  }


  public submitRegister(f : NgForm){
    const user: CreateUserDTO = {
      username: f.value.username,
      email: f.value.email,
      password: f.value.pwd
    }
    this.authService.register(user).subscribe({
      next: res => this.router.navigate(['login']),
      error: err => alert(err)     
  });
  }

}
