import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateQuestionDTO } from 'src/app/models/createquestionDTO.model';
import { Language } from 'src/app/models/language.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService, Pages } from 'src/app/services/notification.service';

@Component({
  selector: 'app-create-question-page',
  templateUrl: './create-question-page.component.html',
  styleUrls: ['./create-question-page.component.scss']
})
export class CreateQuestionPageComponent implements OnInit {

  languages?: Language[];

  constructor(private httpService: HttpService, private router: Router, private notificationService: NotificationService, private authService: AuthenticationService) { }

  ngOnInit(): void {
    this.notificationService.updatePage(Pages.createQuestion);
    this.httpService.getLanguages().subscribe(
      res => this.languages = res
    );
  }

  public onSubmitCreateQuestion(f: NgForm){
    if(this.authService.checkStorageToken()){
      const question:CreateQuestionDTO = {
        title: f.value.title,
        content: f.value.content,
        language: f.value.language
      };
      this.httpService.createNewQuestion(question).subscribe({
        next: res => this.router.navigate(['']),
        error: err => {
          if(f.value.title.slice(-1) != '?'){
            alert('Question should end with an interrogation point : ?')
          }
        }
      });
    }
  }

}
