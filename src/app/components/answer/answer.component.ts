import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Answer } from 'src/app/models/answer.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-answer',
  templateUrl: './answer.component.html',
  styleUrls: ['./answer.component.scss']
})
export class AnswerComponent implements OnInit {

  @Input() answer?: Answer;
  @Input() showQLink: boolean = false;
  @Output('signaled') signalEvent: EventEmitter<void> = new EventEmitter();
  date?: Date;
  isAdmin: boolean = false;
  constructor(public authService: AuthenticationService, private httpService: HttpService, private router: Router) { 
    
  }

  ngOnInit(): void {
    if(this.answer){
      this.date = new Date(this.answer.timestamp*1000);
    }
    const username = this.authService.getCurrentUserName();
    if(username){
      this.authService.getUserByName(username).subscribe(
        u => {
        this.isAdmin = u.authorities[0].authority === 'ADMIN';
        }
    )};
  }

  public toggleSignaled(){
    if(this.answer?.tid){
      this.httpService.signalAnswer(this.answer.tid).subscribe(
        res => this.signalEvent.emit()
      );
    }
  }

  public onDelete(tid: string){
    this.httpService.deleteAnswer(tid).subscribe(
      res => this.signalEvent.emit()
    );
  }

  public goToUser(){
    this.router.navigate(['/users', this.answer?.username])
    .then(() => location.reload())
  }
}
