import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Question } from 'src/app/models/question.model';
import { User } from 'src/app/models/user.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {

  @Input() thumbnail: boolean = true;
  @Input() question?: Question;
  @Output('deleted') signalDeleteEvent: EventEmitter<void> = new EventEmitter();
  @Output('signal') signalEvent: EventEmitter<void> = new EventEmitter();
  date?: Date;
  isAdmin: boolean = false;
  
  constructor(public authService: AuthenticationService, private httpService: HttpService, private router: Router) { }

  ngOnInit(): void {
    if(this.question){
      this.date = new Date(this.question.timestamp*1000);
    }

    const username = this.authService.getCurrentUserName();
    if(username){
      this.authService.getUserByName(username).subscribe(
        u => {
        this.isAdmin = u.authorities[0].authority === 'ADMIN';
        }
    )}; 
  }

  public toggleSignaled(){
    if(this.question?.tid){
      this.httpService.signalQuestion(this.question.tid).subscribe(
        res => this.signalEvent.emit()
      );
    }
  }

  public onDelete(tid: string){
    this.httpService.deleteQuestion(tid).subscribe(
      res => {this.signalEvent.emit()
            this.signalDeleteEvent.emit()}
    );
  }

  public goToUser(){
    this.router.navigate(['/users', this.question?.username])
    .then(() => location.reload())
  }
}
