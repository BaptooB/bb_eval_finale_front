import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UINavComponent } from './uinav.component';

describe('UINavComponent', () => {
  let component: UINavComponent;
  let fixture: ComponentFixture<UINavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UINavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UINavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
