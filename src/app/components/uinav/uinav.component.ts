import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';
import { NotificationService } from 'src/app/services/notification.service';

@Component({
  selector: 'ui-nav',
  templateUrl: './uinav.component.html',
  styleUrls: ['./uinav.component.scss']
})
export class UINavComponent implements OnInit {
  
  @Output('navEvent') navBarEvent: EventEmitter<boolean> = new EventEmitter();
  @Output('loggout') navLogoutEvent: EventEmitter<boolean> = new EventEmitter();

  constructor(private router: Router, public notificationService: NotificationService, private httpService: HttpService, public authService: AuthenticationService) { }

  ngOnInit(): void {

  }

  public onLogin(){
    this.navBarEvent.emit(true);
    this.router.navigate(['login']);
  }

  public onRegister(){
    this.navBarEvent.emit(false);
    this.router.navigate(['register']);
  }

  public onLogout(){
    localStorage.removeItem('username');
    localStorage.removeItem('password');
    this.authService.logout();
    this.notificationService.logout();
    this.router.navigate(['']);
  }

  public onHome(){
    this.router.navigate(['']);
  }

  public onMyPage(){
    this.router.navigate(['/users/' + this.authService.getCurrentUserName()]).then(res => location.reload());
  }

  public onSearchEvent(f: NgForm){
    let title = null;
    
    if(f.valid){
      title = f.value.search;
    }
    this.httpService.searchTitle.next(title);
  }

}
