import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { DetailedQuestion } from 'src/app/models/detailedQuestion.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';


@Component({
  selector: 'app-detailed-question',
  templateUrl: './detailed-question.component.html',
  styleUrls: ['./detailed-question.component.scss']
})
export class DetailedQuestionComponent implements OnInit {

  @Input() question?: DetailedQuestion;
  @Output('signaled') signalEvent: EventEmitter<void> = new EventEmitter(); 
  answerFormVisible: boolean = false;
  isAdmin: boolean = false;
  

  constructor(private httpService: HttpService, public authService: AuthenticationService, private router: Router) { }

  ngOnInit(): void {
    const username = this.authService.getCurrentUserName();
    if(username){
      this.authService.getUserByName(username).subscribe(
        u => {
        this.isAdmin = u.authorities[0].authority === 'ADMIN';
        }
    )};
  }

  public onSignalEvent(){
    this.signalEvent.emit();
  }

  public onSignalCreateEvent(){
    this.answerFormVisible = !this.answerFormVisible;
    this.signalEvent.emit();
  }
  onDelete(){
    this.router.navigate(['']);
  }

  public onAnswer(){
    this.answerFormVisible = !this.answerFormVisible;
    window.scroll({
      top:10000,
      behavior: 'smooth'
    });
  }
}
