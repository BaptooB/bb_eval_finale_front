import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CreateAnswerDTO } from 'src/app/models/createanswerDTO.model';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-create-answer',
  templateUrl: './create-answer.component.html',
  styleUrls: ['./create-answer.component.scss']
})
export class CreateAnswerComponent implements OnInit {

  @Input() questionId?: string
  @Output('answered') answerEvent: EventEmitter<void> = new EventEmitter();
  constructor(private httpService: HttpService, private router: Router, private authService: AuthenticationService) { }

  ngOnInit(): void {
  }

  public onSubmitCreateAnswer(f: NgForm){
    if(this.authService.checkStorageToken()){
      const answer:CreateAnswerDTO = {
        content: f.value.content,
      };
      f.reset();
      if(this.questionId){
        this.httpService.createNewAnswer(answer, this.questionId).subscribe({
          next: res => this.answerEvent.emit()
        });
      }
    }
  }
}
