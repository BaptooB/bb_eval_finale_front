import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateQuestionPageComponent } from './pages/create-question-page/create-question-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';

import { LoginPageComponent } from './pages/login-page/login-page.component';
import { QuestionPageComponent } from './pages/question-page/question-page.component';
import { RegisterPageComponent } from './pages/register-page/register-page.component';
import { UserPageComponent } from './pages/user-page/user-page.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'login', component: LoginPageComponent},
  {path: 'register', component: RegisterPageComponent},
  {path: 'questions/create', component: CreateQuestionPageComponent},
  {path: 'questions/:id', component: QuestionPageComponent},
  {path: 'users/:username', component: UserPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
